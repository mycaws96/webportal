import './App.css';
import { Grid, TextField, Button, Typography } from "@material-ui/core"

function App() {
  return (
    // <div className="App">
    //   <form alignitem="center" justify="center" direction="column">
    //     <div class="mb-3">
    //       <label for="exampleInputEmail1" class="form-label">Email address</label>
    //       <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
    //       <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
    //     </div>
    //     <div class="mb-3">
    //       <label for="exampleInputPassword1" class="form-label">Password</label>
    //       <input type="password" class="form-control" id="exampleInputPassword1" />
    //     </div>
    //     <div class="mb-3 form-check">
    //       <input type="checkbox" class="form-check-input" id="exampleCheck1" />
    //       <label class="form-check-label" for="exampleCheck1">Check me out</label>
    //     </div>
    //     <button type="submit" class="btn btn-primary">Submit</button>
    //   </form>
    // </div>
    <Grid
      container justify="center"
      alignItems="center"
      direction="column"
      style={{ minHeight: "100vh" }}
      spacing={5}>
      <Typography variant="h5" color="primary">
        LOGIN
      </Typography>

      <Grid item style={{ border: "0.2px solid gray" }}>
        <Login />
      </Grid>
    </Grid>

  );
}

export default App;

const Login = () => {
  return (
    <Grid container direction="column" alignItems="center" justify="center">
      <TextField
        variant="outlined"
        label="Email"
        fullWidth style={{ marginBottom: "2rem", marginTop: "1rem" }}
      />
      <TextField
        variant="outlined"
        label="Password"
        fullWidth style={{ marginBottom: "2rem" }} />

      <Button size="large" variant="contained" color="primary">
        LOGIN
      </Button>
    </Grid>);
}
